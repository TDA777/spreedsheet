using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Google.GData.Client;
using Google.GData.Spreadsheets;
using UnityEditor;
using UnityEngine;

namespace Datablocks
{

    /// <summary>
    ///     Editor window for importing spreadsheets from Google sheets
    /// </summary>
    public class ImportGoogleSpreadsheet : DatablockImporter
    {
        private readonly SheetsAPI sheetsAPI = new SheetsAPI();
        private readonly List<SpreadsheetEntry> spreadsheetEntries = new List<SpreadsheetEntry>();

        private bool canBeImported;
        private bool fieldsFoldout;
        private int importDatablockTypeIndex;
        private int nameColumnIndex = -1;
        private int spreadsheetSelectIndex;
        private AtomEntryCollection spreadsheetRows;

        private string uriTable;

        private string tempAuthtoken;
        private bool showTableName = false;

        public string SheetName { get; private set; }

        [MenuItem("Tools/Import Google Spreadsheet")]
        public static void ShowImportWindow()
        {
            GetWindow<ImportGoogleSpreadsheet>("Import Google Spreadsheet");
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            sheetsAPI.Initilize();
        }

        private void OnGUI()
        {
            if (sheetsAPI.HasAccessToken())
                ShowOAuthPanel();
            else
            {
                ShowSpreadsheetList();
            }
        }

        private void ShowSpreadsheetList()
        {

            var textField = EditorPrefs.GetString("MySelectedTable");
            if (!string.IsNullOrEmpty(textField) && string.IsNullOrEmpty(SheetName) && showTableName)
                SheetName = textField;
            EditorGUILayout.HelpBox("Refresh spreadsheets to retrieve a list of sheets associated with the connected Google account. Only the first" +
                                    " sheet in a workbook will be loaded and the first row should contain the field names.", MessageType.None);
            
            EditorGUILayout.BeginHorizontal();
            showTableName = EditorGUILayout.Toggle("Edit name table", showTableName);
            if (showTableName)
            {
                GUI.backgroundColor = new Color(0, 0.2f, .8f);
                SheetName = EditorGUILayout.TextField(SheetName);
                GUI.backgroundColor = Color.white;
            }
            else
            {
                GUI.contentColor = Color.gray;
                EditorGUILayout.LabelField(SheetName);
                GUI.contentColor = Color.white;
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            GUI.backgroundColor = new Color(0, 0.8f, .8f);
            if (GUILayout.Button("Refresh Spreesheets"))
            {
                spreadsheetEntries.Clear();
                var query =  new SpreadsheetQuery();
                query.Title = SheetName;
                query.Exact = true;

                SpreadsheetFeed feed = null;
                try
                {
                    feed = sheetsAPI.Service.Query(query);
                    if (feed.Entries.Count > 0)
                    {
                        showTableName = false;
                        EditorPrefs.SetString("MySelectedTable", SheetName);
                    }
                }
                catch (Exception)
                {
                    Debug.LogError("OAuth error");
                    sheetsAPI.ClearOAuthToken();
                    throw;
                }

                // �������� ����� ��� ������������ ����������� �������
                foreach (SpreadsheetEntry entry in feed.Entries)
                {
                    spreadsheetEntries.Add(entry);
                }
            }

            GUI.backgroundColor = new Color(1f, 0.2f, .4f);

            if (GUILayout.Button("Clear OAuth token"))
            {
                sheetsAPI.ClearOAuthToken();
            }            
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();





            var options = new List<string>(spreadsheetEntries.Select(s => s.Title.Text));

            GUI.enabled = spreadsheetEntries.Count > 0;
            if (spreadsheetEntries.Count == 0)
            {
                options.Insert(0, "No spreadsheets found");
            }
            else if(spreadsheetEntries.Count > 1)
            {
                options.Insert(0, "Select");
            }

            if (spreadsheetSelectIndex >= options.Count)
                spreadsheetSelectIndex = 0;
            int tempSpreadsheetIndex = EditorGUILayout.Popup("Select spreedsheet", spreadsheetSelectIndex, options.ToArray());

            if (tempSpreadsheetIndex != spreadsheetSelectIndex || 1 == options.Count)
            {
                canBeImported = false;

                spreadsheetSelectIndex = tempSpreadsheetIndex;


                GUI.backgroundColor =(spreadsheetEntries.Count > 0) ? Color.green: Color.red;
                if (GUILayout.Button("Read"))
                {
                    SpreadsheetEntry spreadsheet = spreadsheetEntries.FirstOrDefault(s => s.Title.Text == options[spreadsheetSelectIndex]);
                    detectedHeaders = ParseSpreadsheet(spreadsheet);

                    if (detectedHeaders == null)
                    {
                        Debug.LogError("No data detected in sheet");
                        canBeImported = false;
                        return;
                    }

                    if (nameColumnIndex == -1)
                    {
                        Debug.LogError("Sheet must have a header field named 'Name'");
                        canBeImported = false;
                        return;
                    }

                    //importDatablockTypeIndex = detectedHeaders.IndexOf(detectedHeaders.OrderByDescending(d => d.fields.Count).First());

                    //canBeImported = true;
                }
                GUI.enabled = true;
                GUI.backgroundColor = Color.white;
            }

            if (spreadsheetSelectIndex != 0 && canBeImported)
            {
                //importDatablockTypeIndex = EditorGUILayout.Popup("Choose datablock type", importDatablockTypeIndex, detectedHeaders.Select(d => d.datablockType.ToString()).ToArray());

                //DatablockDetectionInfo datablockDetectionInfo = detectedHeaders[importDatablockTypeIndex];

                //fieldsFoldout = EditorGUILayout.Foldout(fieldsFoldout, datablockDetectionInfo.fields.Count + " detected fields");
                //if (fieldsFoldout)
                //{
                //    foreach (FieldInfo field in datablockDetectionInfo.fields)
                //    {
                //        EditorGUILayout.LabelField(field.Name);
                //    }
                //}

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("New datablock path: \"" + newDatablockDir + "\"");

                if (GUILayout.Button("Change"))
                {
                    string path = EditorUtility.OpenFolderPanel("Select path to save new datablock", "Assets", "");
                    if (String.IsNullOrEmpty(path))
                        return;

                    int assetsIndex = path.IndexOf("/Assets", StringComparison.Ordinal);
                    if (assetsIndex == -1)
                    {
                        Debug.LogError("Path must be in the Assets folder");
                        return;
                    }

                    newDatablockDir = path.Substring(assetsIndex + 1);
                    EditorPrefs.SetString("newDatablockDir", newDatablockDir);
                }
                EditorGUILayout.EndHorizontal();

                int existingCount = 0;
                int newCount = 0;
                foreach (ListEntry spreadsheetRow in spreadsheetRows)
                {
                    string name = spreadsheetRow.Elements[nameColumnIndex].Value;
                    //if (DatablockManager.Instance.GetDatablock(name, datablockDetectionInfo.datablockType, true) != null)
                    existingCount++;
                    //else
                    //    newCount++;
                }

                EditorGUILayout.Separator();
                EditorGUILayout.HelpBox("Ready to import " + newCount + " new and " + existingCount + " existing datablocks.", MessageType.Info);
                EditorGUILayout.Separator();

                GUI.backgroundColor = new Color(0, 0.8f, 0);
                if (GUILayout.Button("Import!"))
                {
                    ImportRows();
                    //DatablockManager.Instance.RefreshAssets();
                }
            }
        }


        private void ImportRows(DatablockDetectionInfo datablockDetectionInfo = null)
        {
            foreach (ListEntry spreadsheetRow in spreadsheetRows)
            {
                string name = spreadsheetRow.Elements[nameColumnIndex].Value;

                //Datablock datablock = GetNamedDatablock(datablockDetectionInfo, name);

                foreach (ListEntry.Custom element in spreadsheetRow.Elements)
                {
                    string fieldName = element.LocalName;
                    string fieldValue = element.Value;

                    //ProcessRawField(datablockDetectionInfo, fieldName, datablock, fieldValue);
                }

                //EditorUtility.SetDirty(datablock);
            }

            AssetDatabase.SaveAssets();

            Debug.Log(spreadsheetRows.Count + " datablocks imported");
        }
        /// <summary>
        /// �������� ������ ���������� �������� �� 0 �����
        /// </summary>
        /// <param name="spreadsheet"></param>
        /// <returns></returns>
        private List<string> ParseSpreadsheet(SpreadsheetEntry spreadsheet)
        {
            //wsFeed �����
            WorksheetFeed wsFeed = spreadsheet.Worksheets;

            if (wsFeed.Entries.Count == 0)
                return null;

            var worksheet = (WorksheetEntry)wsFeed.Entries[0];

            if (worksheet.Rows < 2)
                return null;

            // ���������� URL-�����, ��� ������� ������ ������ �� �����.
            AtomLink listFeedLink = worksheet.Links.FindService(GDataSpreadsheetsNameTable.ListRel, null);

            // �������� ���� ������ �� �����.
            var listQuery = new ListQuery(listFeedLink.HRef.ToString());
            ListFeed listFeed = sheetsAPI.Service.Query(listQuery);

            var headers = new List<string>();
            spreadsheetRows = listFeed.Entries;
            nameColumnIndex = -1;

            var row = (ListEntry)spreadsheetRows[0];
            for (int index = 0; index < row.Elements.Count; index++)
            {
                ListEntry.Custom element = row.Elements[index];

                if (element.LocalName.Equals("name", StringComparison.OrdinalIgnoreCase))
                    nameColumnIndex = index;

                headers.Add(element.LocalName);
            }
            return headers;
        }

        private void ShowOAuthPanel()
        {
            EditorGUILayout.HelpBox("To import a spreadsheet you need to obtain a token from Google to allow access to your spreadsheets. Click the button below and paste the code Google provides into the textbox.", MessageType.Info);

            EditorGUILayout.Separator();

            if (GUILayout.Button("Request token"))
            {
                string authorizationUrl = sheetsAPI.AuthURL();
                Application.OpenURL(authorizationUrl);
            }

            EditorGUILayout.Separator();

            EditorGUIUtility.labelWidth = 85;

            EditorGUILayout.BeginHorizontal();
            tempAuthtoken = EditorGUILayout.TextField("OAuth Token:", tempAuthtoken);


            if (GUILayout.Button("OK", EditorStyles.miniButton, GUILayout.MaxWidth(50)))
            {
                sheetsAPI.SetAccessCode(tempAuthtoken);

                Debug.Log("Access token recieved");
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}